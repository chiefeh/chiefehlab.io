---
icon: material/format-list-bulleted
---

# Hello World

Welcome to my personal reference site! It's been a while but the ground work is here so I will make 
use of it. 

I need to keep things to hand, and generally practice things how I advocate others do things.
[Markdown](https://www.markdownguide.org/cheat-sheet/) is used to manage the site, with 
[MKDocs](https://www.mkdocs.org/) and [Material theme](https://squidfunk.github.io/mkdocs-material/)

## To Do List

- [X] Setup and document the site
- [ ] Basic SSH,Git Operations and VS Code Setup
- [ ] Setup of a Enterprise Linux 9 (Rocky!) Desktop using Ansible
- [ ] Kickstart deployment and creation of ISO/USB Stick for EL9 (Rocky!)
- [ ] Management of a Linux server using ansible-pull
- [ ] Deployment of a Juniper SRX firewall using automation